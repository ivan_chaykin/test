FROM anapsix/alpine-java
LABEL maintainer="deriansv@gmail.com"
COPY /target/test-0.0.1-SNAPSHOT.jar /home/test-0.0.1-SNAPSHOT.jar
CMD ["java","-jar","/home/test-0.0.1-SNAPSHOT.jar"]