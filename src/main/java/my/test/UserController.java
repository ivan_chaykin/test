package my.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Derians
 * Date: 03.10.2018
 * Time: 15:42
 */
@Controller
@RequestMapping(path="/api")
public class UserController {

    @GetMapping(path="/user")
    public @ResponseBody
    User getUser() {

        return new User(1L, "Ivan", "user@email.com");
    }



}
